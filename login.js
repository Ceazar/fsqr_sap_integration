const axios = require('axios');
var https = require('https');
require('tls').DEFAULT_MIN_VERSION = 'TLSv1'
const dotenv = require('dotenv')
dotenv.config();

const CompanyDB = process.env.CompanyDB;
const Password = process.env.Password;
const UserName = process.env.UName;

const url = process.env.url;

const SAPLOGIN = {
  login: async () => {
    try {
      const res = await axios({
        method: "POST",
        url: `${url}/Login`,
        data: {
          CompanyDB,
          Password,
          UserName
        },
        headers: {
          "Content-Type": "application/json"
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });
      const data = res.data;
      return data;
    } catch (e) {
      console.log("Error login: ", e.response.data);
    }
  }
}

module.exports = SAPLOGIN;
