--a
Content-Type: multipart/mixed;boundary=b

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserTablesMD

{
    "TableName": "FSQR_dr_weights",
    "TableDescription": "FSQRDRNewWeights",
    "TableType": "bott_NoObject"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
    "Name": "transaction_id",
    "Type": "db_Alpha",
    "Size": 254,
    "Description": "Transaction Id",
    "TableName": "@FSQR_dr_weights"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
    "Name": "net_weight",
    "Type": "db_Float",
    "EditSize": 10,
    "SubType": "st_Sum",
    "Description": "Net",
    "TableName": "@FSQR_dr_weights"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
    "Name": "created_by",
    "Type": "db_Alpha",
    "Size": 254,
    "Description": "FSQRCREATEDBY",
    "TableName": "@FSQR_dr_weights"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
    "Name": "created_date",
    "Type": "db_Date",
    "Description": "FSQRCREATEDDATE", 
    "TableName": "@FSQR_dr_weights"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
    "Name": "created_time",
    "Type": "db_Date",
    "SubType": "st_Time",
    "Description": "FSQRCREATEDTIME", 
    "TableName": "@FSQR_dr_weights"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
    "Name": "updated_by",
    "Type": "db_Alpha",
    "Size": 254,
    "Description": "FSQRUPDATEDBY",
    "TableName": "@FSQR_dr_weights"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
    "Name": "updated_date",
    "Type": "db_Date",
    "Description": "FSQRUPDATEDDATE",
    "TableName": "@FSQR_dr_weights"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
    "Name": "updated_time",
    "Type": "db_Date",
    "SubType": "st_Time",
    "Description": "FSQRUPDATEDTIME",
    "TableName": "@FSQR_dr_weights"
}

--b--
--a--