--a
Content-Type: multipart/mixed;boundary=b

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserTablesMD

{
"TableName": "FSQR_PAD",
"TableDescription": "FSQR_PA_Dispo",
"TableType": "bott_NoObject"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "physical_analysis_id",
"Type": "db_Alpha",
"Size": 254,
"Description": "FSQR_PA_ID",
"TableName": "@FSQR_PAD"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
"Name": "disposition_id",
"Type": "db_Alpha",
"Size": 254,
"Description": "FSQR_DIPSO_ID",
"TableName": "@FSQR_PAD"
}


--b--
--a--