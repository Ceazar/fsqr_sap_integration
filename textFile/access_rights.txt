--a
Content-Type: multipart/mixed;boundary=b

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserTablesMD

{
    "TableName": "FSQR_acc_rights",
    "TableDescription": "FSQRAccessRights",
    "TableType": "bott_NoObject"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
    "Name": "action_id",
    "Type": "db_Alpha",
    "Size": 254,
    "Description": "FSQRActionID",
    "TableName": "@FSQR_acc_rights"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
    "Name": "role_id",
    "Type": "db_Alpha",
    "Size": 254,
    "Description": "FSQRRoleID",
    "TableName": "@FSQR_acc_rights"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
    "Name": "status",
    "Type": "db_Alpha",
    "Size": 254,
    "Description": "FSQRARStatus",
    "TableName": "@FSQR_acc_rights"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
    "Name": "created_by",
    "Type": "db_Alpha",
    "Size": 254,
    "Description": "FSQRCREATEDBY",
    "TableName": "@FSQR_acc_rights"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
    "Name": "created_date",
    "Type": "db_Date",
    "Description": "FSQRCREATEDDATE", 
    "TableName": "@FSQR_acc_rights"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
    "Name": "created_time",
    "Type": "db_Date",
    "SubType": "st_Time",
    "Description": "FSQRCREATEDTIME", 
    "TableName": "@FSQR_acc_rights"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
    "Name": "updated_by",
    "Type": "db_Alpha",
    "Size": 254,
    "Description": "FSQRUPDATEDBY",
    "TableName": "@FSQR_acc_rights"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
    "Name": "updated_date",
    "Type": "db_Date",
    "Description": "FSQRUPDATEDDATE",
    "TableName": "@FSQR_acc_rights"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
    "Name": "updated_time",
    "Type": "db_Date",
    "SubType": "st_Time",
    "Description": "FSQRUPDATEDTIME",
    "TableName": "@FSQR_acc_rights"
}

--b--
--a--