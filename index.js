const SAPLOGIN = require("./login");
const batch = require("./batch");
const seeds = require("./seed");

const delay = (sec) => {
  return new Promise((resolve) => setTimeout(resolve, sec));
};

const app = async () => {
  const loginDetails = await SAPLOGIN.login();

  const info = {
    B1SESSION: loginDetails.SessionId,
    ROUTEID: ".node1",
    CompanyDB: process.env.CompanyDB,
  };

  // # delete all UDT
  // await delay(2000);
  // await batch.batchDeleteAllUDT({ info });
  
  // await delay(2000);
  // await batch.batchModules({ info });

  // await delay(2000);
  // await batch.batchActions({ info });

  // await delay(2000);
  // await batch.batchRoles({ info });

  // await delay(2000);
  // await batch.batchAccessRights({ info });

  // await delay(2000);
  // await batch.batchDepartments({ info });

  // await delay(2000);
  // await batch.batchUser({ info });

  // await delay(2000);
  // await batch.batchDisposition({ info });

  // await delay(2000);
  // await batch.batchRemarks({ info });

  // await delay(2000);
  // await batch.batchFSQRRECON({ info });

  // await delay(2000);
  // await batch.batchPriceReduction({ info });

  // await delay(2000);
  // await batch.batchMRR({ info });

  // await delay(2000);
  // await batch.batchCHEMICAL({ info });

  // await delay(2000);
  // await batch.batchMTS({ info });

  // await delay(2000);
  // await batch.batchPHYSICAL({ info });

  // await delay(2000);
  // await batch.batchCAD({ info });

  // await delay(2000);
  // await batch.batchPAD({ info });

  // await delay(2000);
  // await batch.batchPAR({ info });

  // await delay(2000);
  // await batch.batchFSQRRMAF({ info });

  // await delay(2000);
  // await batch.batchTransactionType({ info });

  // await delay(2000);
  // await batch.batchStatus({ info });

  // await delay(2000);
  // await batch.batchDR({ info });

  // await delay(2000);
  // await batch.batchBIR({ info });

  // await delay(2000);
  // await batch.batchFSQRTransaction({ info });

  // await delay(2000);
  // await batch.batchRejectionReceipt({ info });

  // await delay(2000);
  // await batch.batchCriteria({ info });

  // await delay(2000);
  // await batch.batchDeductionReceipt({ info });

  // await delay(2000);
  // await batch.batchDeductionCriteria({ info });

  // await delay(2000);
  // await batch.batchTransactionStatusLog({ info });

  // await delay(2000);
  // await batch.batchBusinessEntity({ info });

  // await delay(2000);
  // await batch.batchItems({ info });

  // await delay(2000);
  // await batch.batchRawMaterials({ info });

  // await delay(2000);
  // await batch.batchMicro({ info });

  // await delay(2000);
  // await batch.batchMicroDetails({ info });

  // await delay(2000);
  // await batch.batchLotNum({ info });

  // await delay(2000);
  // await batch.batchDeviceConfig({ info });

  // await delay(2000);
  // await batch.batchCompany({ info });

  // await delay(2000);
  // await batch.batchNewWeights({ info });

  // await delay(2000);
  // await batch.batchActivityLogs({ info });

  // await delay(2000);
  // await batch.batchActivityLogs({ info });

  // await delay(2000);
  // await batch.batchTable({ info }, '--Company--', 'company');

  // await delay(2000);
  // await batch.batchTable({ info }, '--DRWeigths--', 'dr-new-weights');

  // await delay(2000);
  // await batch.batchTable({ info }, '--NotifReads--', 'notif_read');

  // await delay(2000);
  // await batch.batchTable({ info }, '--Notifs--', 'notifs');

  // await delay(2000);
  await batch.batchTable({ info }, '--Specifications--', 'specs');

  await delay(2000);
  await batch.batchTable({ info }, '--MRRSpecs--', 'mrr_specs');

};

app();

const seeders = async () => {
  const loginDetails = await SAPLOGIN.login();

  const info = {
    B1SESSION: loginDetails.SessionId,
    ROUTEID: ".node1",
    CompanyDB: process.env.CompanyDB,
  };

  // await delay(2000);
  await seeds.seedSpecs({ info });

  // await delay(2000);
  // await seeds.seedModules({ info });

  // await delay(2000);
  // await seeds.seedActions({ info });

  // await delay(2000);
  // await seeds.seedRoles({ info });

  // await delay(2000);
  // await seeds.seedDepartments({ info });

  // await delay(2000);
  // await seeds.seedUsers({ info });

  // await delay(2000);
  // await seeds.seedAccessRights({ info });

  // await delay(2000);
  // await seeds.seedBE({ info });

  // await delay(2000);
  // await seeds.seedRawMaterials({ info });

  // await delay(2000);
  // await seeds.seedBIR({ info });

  // await delay(2000);
  // await seeds.seedItems({ info });

  // await delay(2000);
  // await seeds.seedCriteria({ info });

  // await delay(2000);
  // await seeds.seedDispo({ info });

  // await delay(2000);
  // await seeds.seedRemarks({ info });

  // await delay(2000);
  // await seeds.seedStatus({ info });

  // await delay(2000);
  // await seeds.seedTransType({ info });

  // await delay(2000);
  // await seeds.seedCompany({ info });
};

// seeders();

const drop = async () => {
  const loginDetails = await SAPLOGIN.login();

  const info = {
    B1SESSION: loginDetails.SessionId,
    ROUTEID: ".node1",
    CompanyDB: process.env.CompanyDB,
  };

  //drop

  // await delay(2000);
  // await batch.batchDrop({ info });

  //--drop
};

// drop();
