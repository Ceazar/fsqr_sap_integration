const axios = require('axios');
var https = require('https');
const fs = require('fs');
require('tls').DEFAULT_MIN_VERSION = 'TLSv1'

const url = process.env.url;

const readTextFile = async path => {
  try {
    const data = fs.readFileSync(path, "utf8");
    return data;
  } catch (e) {
    console.log("Error: ", e);
  }
}

const seeds = {
  seedSpecs: async ({ info }) => {
    try {
      const msg = await readTextFile("./seedTextFile/specs.txt");
      const res = await axios.post(`${url}/$batch`, msg, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `CompanyDB=${info.CompanyDB};B1SESSION=${info.B1SESSION};ROUTEID=${info.ROUTEID}`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });
      const data = {
        desc: "--Seed Modules--",
        reqStatus: res.status,
        reqMsg: res.statusText,
        response: res.data
      };
      console.log(data);
    } catch (e) {
      console.log("Error: ", e.response.data);
    }
  },
  seedModules: async ({ info }) => {
    console.log(info)
    try {
      const msg = await readTextFile("./seedTextFile/modules.txt");
      const res = await axios.post(`${url}/$batch`, msg, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `CompanyDB=${info.CompanyDB};B1SESSION=${info.B1SESSION};ROUTEID=${info.ROUTEID}`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });
      const data = {
        desc: "--Seed Modules--",
        reqStatus: res.status,
        reqMsg: res.statusText,
        response: res.data
      };
      console.log(data);
    } catch (e) {
      console.log("Error: ", e.response.data);
    }
  },
  seedActions: async ({ info }) => {
    console.log(info)
    try {
      const msg = await readTextFile("./seedTextFile/actions.txt");
      const res = await axios.post(`${url}/$batch`, msg, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `CompanyDB=${info.CompanyDB};B1SESSION=${info.B1SESSION};ROUTEID=${info.ROUTEID}`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });
      const data = {
        desc: "--Seed Actions--",
        reqStatus: res.status,
        reqMsg: res.statusText,
        response: res.data
      };
      console.log(data);
    } catch (e) {
      console.log("Error: ", e.response.data);
    }
  },
  seedRoles: async ({ info }) => {
    console.log(info)
    try {
      const msg = await readTextFile("./seedTextFile/roles.txt");
      const res = await axios.post(`${url}/$batch`, msg, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `CompanyDB=${info.CompanyDB};B1SESSION=${info.B1SESSION};ROUTEID=${info.ROUTEID}`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });
      const data = {
        desc: "--Seed Roles--",
        reqStatus: res.status,
        reqMsg: res.statusText,
        response: res.data
      };
      console.log(data);
    } catch (e) {
      console.log("Error: ", e.response.data);
    }
  },
  seedDepartments: async ({ info }) => {
    console.log(info)
    try {
      const msg = await readTextFile("./seedTextFile/departments.txt");
      const res = await axios.post(`${url}/$batch`, msg, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `CompanyDB=${info.CompanyDB};B1SESSION=${info.B1SESSION};ROUTEID=${info.ROUTEID}`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });
      const data = {
        desc: "--Seed Departments--",
        reqStatus: res.status,
        reqMsg: res.statusText,
        response: res.data
      };
      console.log(data);
    } catch (e) {
      console.log("Error: ", e.response.data);
    }
  },
  seedUsers: async ({ info }) => {
    console.log(info)
    try {
      const msg = await readTextFile("./seedTextFile/users.txt");
      const res = await axios.post(`${url}/$batch`, msg, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `CompanyDB=${info.CompanyDB};B1SESSION=${info.B1SESSION};ROUTEID=${info.ROUTEID}`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });
      const data = {
        desc: "--Seed User--",
        reqStatus: res.status,
        reqMsg: res.statusText,
        response: res.data
      };
      console.log(data);
    } catch (e) {
      console.log("Error: ", e.response.data);
    }
  },
  seedAccessRights: async ({ info }) => {
    console.log(info)
    try {
      const msg = await readTextFile("./seedTextFile/access_rights.txt");
      const res = await axios.post(`${url}/$batch`, msg, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `CompanyDB=${info.CompanyDB};B1SESSION=${info.B1SESSION};ROUTEID=${info.ROUTEID}`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });
      const data = {
        desc: "--Seed Access Rights--",
        reqStatus: res.status,
        reqMsg: res.statusText,
        response: res.data
      };
      console.log(data);
    } catch (e) {
      console.log("Error: ", e.response.data);
    }
  },
  seedBE: async ({ info }) => {
    console.log(info)
    try {
      const msg = await readTextFile("./seedTextFile/business_entity.txt");
      const res = await axios.post(`${url}/$batch`, msg, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `CompanyDB=${info.CompanyDB};B1SESSION=${info.B1SESSION};ROUTEID=${info.ROUTEID}`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });
      const data = {
        desc: "--Seed business_entity--",
        reqStatus: res.status,
        reqMsg: res.statusText,
        response: res.data
      };
      console.log(data);
    } catch (e) {
      console.log("Error: ", e.response.data);
    }
  },
  seedRawMaterials: async ({ info }) => {
    console.log(info)
    try {
      const msg = await readTextFile("./seedTextFile/raw_materials.txt");
      const res = await axios.post(`${url}/$batch`, msg, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `CompanyDB=${info.CompanyDB};B1SESSION=${info.B1SESSION};ROUTEID=${info.ROUTEID}`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });
      const data = {
        desc: "--Seed raw_materials--",
        reqStatus: res.status,
        reqMsg: res.statusText,
        response: res.data
      };
      console.log(data);
    } catch (e) {
      console.log("Error: ", e.response.data);
    }
  },
  seedBIR: async ({ info }) => {
    console.log(info)
    try {
      const msg = await readTextFile("./seedTextFile/bir.txt");
      const res = await axios.post(`${url}/$batch`, msg, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `CompanyDB=${info.CompanyDB};B1SESSION=${info.B1SESSION};ROUTEID=${info.ROUTEID}`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });
      const data = {
        desc: "--Seed BIR--",
        reqStatus: res.status,
        reqMsg: res.statusText,
        response: res.data
      };
      console.log(data);
    } catch (e) {
      console.log("Error: ", e.response.data);
    }
  },
  seedItems: async ({ info }) => {
    console.log(info)
    try {
      const msg = await readTextFile("./seedTextFile/items.txt");
      const res = await axios.post(`${url}/$batch`, msg, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `CompanyDB=${info.CompanyDB};B1SESSION=${info.B1SESSION};ROUTEID=${info.ROUTEID}`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });
      const data = {
        desc: "--Seed items--",
        reqStatus: res.status,
        reqMsg: res.statusText,
        response: res.data
      };
      console.log(data);
    } catch (e) {
      console.log("Error: ", e.response.data);
    }
  },
  seedCriteria: async ({ info }) => {
    console.log(info)
    try {
      const msg = await readTextFile("./seedTextFile/criteria.txt");
      const res = await axios.post(`${url}/$batch`, msg, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `CompanyDB=${info.CompanyDB};B1SESSION=${info.B1SESSION};ROUTEID=${info.ROUTEID}`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });
      const data = {
        desc: "--Seed Criteria--",
        reqStatus: res.status,
        reqMsg: res.statusText,
        response: res.data
      };
      console.log(data);
    } catch (e) {
      console.log("Error: ", e.response.data);
    }
  },
  seedDispo: async ({ info }) => {
    console.log(info)
    try {
      const msg = await readTextFile("./seedTextFile/disposition.txt");
      const res = await axios.post(`${url}/$batch`, msg, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `CompanyDB=${info.CompanyDB};B1SESSION=${info.B1SESSION};ROUTEID=${info.ROUTEID}`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });
      const data = {
        desc: "--Seed disposition--",
        reqStatus: res.status,
        reqMsg: res.statusText,
        response: res.data
      };
      console.log(data);
    } catch (e) {
      console.log("Error: ", e.response.data);
    }
  },
  seedRemarks: async ({ info }) => {
    console.log(info)
    try {
      const msg = await readTextFile("./seedTextFile/remarks.txt");
      const res = await axios.post(`${url}/$batch`, msg, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `CompanyDB=${info.CompanyDB};B1SESSION=${info.B1SESSION};ROUTEID=${info.ROUTEID}`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });
      const data = {
        desc: "--Seed remarks--",
        reqStatus: res.status,
        reqMsg: res.statusText,
        response: res.data
      };
      console.log(data);
    } catch (e) {
      console.log("Error: ", e.response.data);
    }
  },
  seedStatus: async ({ info }) => {
    console.log(info)
    try {
      const msg = await readTextFile("./seedTextFile/status.txt");
      const res = await axios.post(`${url}/$batch`, msg, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `CompanyDB=${info.CompanyDB};B1SESSION=${info.B1SESSION};ROUTEID=${info.ROUTEID}`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });
      const data = {
        desc: "--Seed status--",
        reqStatus: res.status,
        reqMsg: res.statusText,
        response: res.data
      };
      console.log(data);
    } catch (e) {
      console.log("Error: ", e.response.data);
    }
  },
  seedTransType: async ({ info }) => {
    console.log(info)
    try {
      const msg = await readTextFile("./seedTextFile/transaction_type.txt");
      const res = await axios.post(`${url}/$batch`, msg, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `CompanyDB=${info.CompanyDB};B1SESSION=${info.B1SESSION};ROUTEID=${info.ROUTEID}`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });
      const data = {
        desc: "--Seed transaction_type--",
        reqStatus: res.status,
        reqMsg: res.statusText,
        response: res.data
      };
      console.log(data);
    } catch (e) {
      console.log("Error: ", e.response.data);
    }
  },
  seedCompany: async ({ info }) => {
    console.log(info)
    try {
      const msg = await readTextFile("./seedTextFile/company.txt");
      const res = await axios.post(`${url}/$batch`, msg, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `CompanyDB=${info.CompanyDB};B1SESSION=${info.B1SESSION};ROUTEID=${info.ROUTEID}`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });
      const data = {
        desc: "--Seed company--",
        reqStatus: res.status,
        reqMsg: res.statusText,
        response: res.data
      };
      console.log(data);
    } catch (e) {
      console.log("Error: ", e.response.data);
    }
  },
}

module.exports = seeds;