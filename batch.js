const axios = require("axios");
var https = require("https");
const fs = require("fs");
require("tls").DEFAULT_MIN_VERSION = "TLSv1";

const url = process.env.url;

const readTextFile = async (path) => {
  try {
    const data = fs.readFileSync(path, "utf8");
    return data;
  } catch (e) {
    console.log("Error: ", e);
  }
};

const batch = {
  batchTable: async ({ info }, desc, file) => {
    try {
      const msg = await readTextFile(`./textFile/${file}.txt`);
      const res = await axios.post(`${url}/$batch`, msg, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `CompanyDB=${info.CompanyDB};B1SESSION=${info.B1SESSION};ROUTEID=${info.ROUTEID}`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });
      const data = {
        desc,
        reqStatus: res.status,
        reqMsg: res.statusText,
        response: res.data
      };
      if (data.response) {
        console.log(data.response);
      }
    } catch (e) {
      console.log("Error: ", e.response.data);
    }
  },
  batchModules: async ({ info }) => {
    console.log(info);
    try {
      const msg = await readTextFile("./textFile/modules.txt");
      const res = await axios.post(`${url}/$batch`, msg, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `CompanyDB=${info.CompanyDB};B1SESSION=${info.B1SESSION};ROUTEID=${info.ROUTEID}`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      const data = {
        desc: "--Modules--",
        reqStatus: res.status,
        reqMsg: res.statusText,
        response: res.data,
      };
      console.log(data);
    } catch (e) {
      console.log("Error: ", e.response.data);
    }
  },
  batchActions: async ({ info }) => {
    console.log(info);
    try {
      const msg = await readTextFile("./textFile/actions.txt");
      const res = await axios.post(`${url}/$batch`, msg, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `CompanyDB=${info.CompanyDB};B1SESSION=${info.B1SESSION};ROUTEID=${info.ROUTEID}`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      const data = {
        desc: "--Actions--",
        reqStatus: res.status,
        reqMsg: res.statusText,
        response: res.data,
      };
      console.log(data);
    } catch (e) {
      console.log("Error: ", e.response.data);
    }
  },
  batchRoles: async ({ info }) => {
    console.log(info);
    try {
      const msg = await readTextFile("./textFile/roles.txt");
      const res = await axios.post(`${url}/$batch`, msg, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `CompanyDB=${info.CompanyDB};B1SESSION=${info.B1SESSION};ROUTEID=${info.ROUTEID}`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      const data = {
        desc: "--Roles--",
        reqStatus: res.status,
        reqMsg: res.statusText,
        response: res.data,
      };
      console.log(data);
    } catch (e) {
      console.log("Error: ", e.response.data);
    }
  },
  batchAccessRights: async ({ info }) => {
    console.log(info);
    try {
      const msg = await readTextFile("./textFile/access_rights.txt");
      const res = await axios.post(`${url}/$batch`, msg, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `CompanyDB=${info.CompanyDB};B1SESSION=${info.B1SESSION};ROUTEID=${info.ROUTEID}`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      const data = {
        desc: "--AccessRights--",
        reqStatus: res.status,
        reqMsg: res.statusText,
        response: res.data,
      };
      console.log(data);
    } catch (e) {
      console.log("Error: ", e.response.data);
    }
  },
  batchDepartments: async ({ info }) => {
    console.log(info);
    try {
      const msg = await readTextFile("./textFile/departments.txt");
      const res = await axios.post(`${url}/$batch`, msg, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `CompanyDB=${info.CompanyDB};B1SESSION=${info.B1SESSION};ROUTEID=${info.ROUTEID}`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      const data = {
        desc: "--Departments--",
        reqStatus: res.status,
        reqMsg: res.statusText,
        response: res.data,
      };
      console.log(data);
    } catch (e) {
      console.log("Error: ", e.response.data);
    }
  },
  batchUser: async ({ info }) => {
    console.log(info);
    try {
      const msg = await readTextFile("./textFile/user.txt");
      const res = await axios.post(`${url}/$batch`, msg, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `CompanyDB=${info.CompanyDB};B1SESSION=${info.B1SESSION};ROUTEID=${info.ROUTEID}`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      const data = {
        desc: "--User--",
        reqStatus: res.status,
        reqMsg: res.statusText,
        response: res.data,
      };
      console.log(data);
    } catch (e) {
      console.log("Error: ", e.response.data);
    }
  },

  batchFSQRTransaction: async ({ info }) => {
    console.log(info);
    try {
      const msg = await readTextFile("./textFile/transactions.txt");
      const res = await axios.post(`${url}/$batch`, msg, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `CompanyDB=${info.CompanyDB};B1SESSION=${info.B1SESSION};ROUTEID=${info.ROUTEID}`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      const data = {
        desc: "--Transaction--",
        reqStatus: res.status,
        reqMsg: res.statusText,
        response: res.data,
      };
      console.log(data);
    } catch (e) {
      console.log("Error: ", e.response.data);
    }
  },
  batchFSQRRMAF: async ({ info }) => {
    console.log(info);
    try {
      const msg = await readTextFile("./textFile/rmaf.txt");
      const res = await axios.post(`${url}/$batch`, msg, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `CompanyDB=${info.CompanyDB};B1SESSION=${info.B1SESSION};ROUTEID=${info.ROUTEID}`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      const data = {
        desc: "--RMAF--",
        reqStatus: res.status,
        reqMsg: res.statusText,
        response: res.data,
      };
      console.log(data);
    } catch (e) {
      console.log("Error: ", e.response.data);
    }
  },
  batchFSQRRECON: async ({ info }) => {
    try {
      const msg = await readTextFile("./textFile/reconsideration.txt");
      const res = await axios.post(`${url}/$batch`, msg, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `CompanyDB=${info.CompanyDB};B1SESSION=${info.B1SESSION};ROUTEID=${info.ROUTEID}`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      const data = {
        desc: "--Reconsideration--",
        reqStatus: res.status,
        reqMsg: res.statusText,
        response: res.data,
      };
      console.log(data);
    } catch (e) {
      console.log("Error: ", e.response.data);
    }
  },
  batchPHYSICAL: async ({ info }) => {
    console.log(info);
    try {
      const msg = await readTextFile("./textFile/physical_analysis.txt");
      const res = await axios.post(`${url}/$batch`, msg, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `CompanyDB=${info.CompanyDB};B1SESSION=${info.B1SESSION};ROUTEID=${info.ROUTEID}`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      const data = {
        desc: "--PhysicalAnalysis--",
        reqStatus: res.status,
        reqMsg: res.statusText,
        response: res.data,
      };
      console.log(data);
    } catch (e) {
      console.log("Error: ", e.response.data);
    }
  },
  batchCHEMICAL: async ({ info }) => {
    console.log(info);
    try {
      const msg = await readTextFile("./textFile/chemical_analysis.txt");
      const res = await axios.post(`${url}/$batch`, msg, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `CompanyDB=${info.CompanyDB};B1SESSION=${info.B1SESSION};ROUTEID=${info.ROUTEID}`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      const data = {
        desc: "--ChemicalAnalysis--",
        reqStatus: res.status,
        reqMsg: res.statusText,
        response: res.data,
      };
      console.log(data);
    } catch (e) {
      console.log("Error: ", e.response.data);
    }
  },
  batchPriceReduction: async ({ info }) => {
    console.log(info);
    try {
      const msg = await readTextFile("./textFile/price_reduction.txt");
      const res = await axios.post(`${url}/$batch`, msg, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `CompanyDB=${info.CompanyDB};B1SESSION=${info.B1SESSION};ROUTEID=${info.ROUTEID}`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      const data = {
        desc: "--PriceReduction--",
        reqStatus: res.status,
        reqMsg: res.statusText,
        response: res.data,
      };
      console.log(data);
    } catch (e) {
      console.log("Error: ", e.response.data);
    }
  },
  batchBIR: async ({ info }) => {
    console.log(info);
    try {
      const msg = await readTextFile("./textFile/bir.txt");
      const res = await axios.post(`${url}/$batch`, msg, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `CompanyDB=${info.CompanyDB};B1SESSION=${info.B1SESSION};ROUTEID=${info.ROUTEID}`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      const data = {
        desc: "--BIR--",
        reqStatus: res.status,
        reqMsg: res.statusText,
        response: res.data,
      };
      console.log(data);
    } catch (e) {
      console.log("Error: ", e.response.data);
    }
  },
  batchDR: async ({ info }) => {
    console.log(info);
    try {
      const msg = await readTextFile("./textFile/delivery_receipt.txt");
      const res = await axios.post(`${url}/$batch`, msg, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `CompanyDB=${info.CompanyDB};B1SESSION=${info.B1SESSION};ROUTEID=${info.ROUTEID}`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      const data = {
        desc: "--FSQR--",
        reqStatus: res.status,
        reqMsg: res.statusText,
        response: res.data,
      };
      console.log(data);
    } catch (e) {
      console.log("Error: ", e.response.data);
    }
  },
  batchStatus: async ({ info }) => {
    console.log(info);
    try {
      const msg = await readTextFile("./textFile/status.txt");
      const res = await axios.post(`${url}/$batch`, msg, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `CompanyDB=${info.CompanyDB};B1SESSION=${info.B1SESSION};ROUTEID=${info.ROUTEID}`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      const data = {
        desc: "--Status--",
        reqStatus: res.status,
        reqMsg: res.statusText,
        response: res.data,
      };
      console.log(data);
    } catch (e) {
      console.log("Error: ", e.response.data);
    }
  },
  batchRejectionReceipt: async ({ info }) => {
    console.log(info);
    try {
      const msg = await readTextFile("./textFile/rejection_receipt.txt");
      const res = await axios.post(`${url}/$batch`, msg, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `CompanyDB=${info.CompanyDB};B1SESSION=${info.B1SESSION};ROUTEID=${info.ROUTEID}`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      const data = {
        desc: "--RejectionReceipt--",
        reqStatus: res.status,
        reqMsg: res.statusText,
        response: res.data,
      };
      console.log(data);
    } catch (e) {
      console.log("Error: ", e.response.data);
    }
  },
  batchMTS: async ({ info }) => {
    console.log(info);
    try {
      const msg = await readTextFile("./textFile/moisture_tally_sheet.txt");
      const res = await axios.post(`${url}/$batch`, msg, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `CompanyDB=${info.CompanyDB};B1SESSION=${info.B1SESSION};ROUTEID=${info.ROUTEID}`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      const data = {
        desc: "--MTS--",
        reqStatus: res.status,
        reqMsg: res.statusText,
        response: res.data,
      };
      console.log(data);
    } catch (e) {
      console.log("Error: ", e.response.data);
    }
  },
  batchRemarks: async ({ info }) => {
    console.log(info);
    try {
      const msg = await readTextFile("./textFile/remarks.txt");
      const res = await axios.post(`${url}/$batch`, msg, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `CompanyDB=${info.CompanyDB};B1SESSION=${info.B1SESSION};ROUTEID=${info.ROUTEID}`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      const data = {
        desc: "--Remarks--",
        reqStatus: res.status,
        reqMsg: res.statusText,
        response: res.data,
      };
      console.log(data);
    } catch (e) {
      console.log("Error: ", e.response.data);
    }
  },
  batchDisposition: async ({ info }) => {
    console.log(info);
    try {
      const msg = await readTextFile("./textFile/disposition.txt");
      const res = await axios.post(`${url}/$batch`, msg, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `CompanyDB=${info.CompanyDB};B1SESSION=${info.B1SESSION};ROUTEID=${info.ROUTEID}`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      const data = {
        desc: "--Disposition--",
        reqStatus: res.status,
        reqMsg: res.statusText,
        response: res.data,
      };
      console.log(data);
    } catch (e) {
      console.log("Error: ", e.response.data);
    }
  },
  batchMRR: async ({ info }) => {
    console.log(info);
    try {
      const msg = await readTextFile(
        "./textFile/material_rejection_report.txt"
      );
      const res = await axios.post(`${url}/$batch`, msg, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `CompanyDB=${info.CompanyDB};B1SESSION=${info.B1SESSION};ROUTEID=${info.ROUTEID}`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      const data = {
        desc: "--MRR--",
        reqStatus: res.status,
        reqMsg: res.statusText,
        response: res.data,
      };
      console.log(data);
    } catch (e) {
      console.log("Error: ", e.response.data);
    }
  },
  batchCriteria: async ({ info }) => {
    console.log(info);
    try {
      const msg = await readTextFile("./textFile/criteria.txt");
      const res = await axios.post(`${url}/$batch`, msg, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `CompanyDB=${info.CompanyDB};B1SESSION=${info.B1SESSION};ROUTEID=${info.ROUTEID}`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      const data = {
        desc: "--Criteria--",
        reqStatus: res.status,
        reqMsg: res.statusText,
        response: res.data,
      };
      console.log(data);
    } catch (e) {
      console.log("Error: ", e.response.data);
    }
  },
  batchDeductionReceipt: async ({ info }) => {
    console.log(info);
    try {
      const msg = await readTextFile("./textFile/deduction_receipt.txt");
      const res = await axios.post(`${url}/$batch`, msg, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `CompanyDB=${info.CompanyDB};B1SESSION=${info.B1SESSION};ROUTEID=${info.ROUTEID}`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      const data = {
        desc: "--DeductionReceipt--",
        reqStatus: res.status,
        reqMsg: res.statusText,
        response: res.data,
      };
      console.log(data);
    } catch (e) {
      console.log("Error: ", e.response.data);
    }
  },
  batchDeductionCriteria: async ({ info }) => {
    console.log(info);
    try {
      const msg = await readTextFile("./textFile/deduction_criteria.txt");
      const res = await axios.post(`${url}/$batch`, msg, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `CompanyDB=${info.CompanyDB};B1SESSION=${info.B1SESSION};ROUTEID=${info.ROUTEID}`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      const data = {
        desc: "--DeductionCriteria--",
        reqStatus: res.status,
        reqMsg: res.statusText,
        response: res.data,
      };
      console.log(data);
    } catch (e) {
      console.log("Error: ", e.response.data);
    }
  },
  batchTransactionType: async ({ info }) => {
    console.log(info);
    try {
      const msg = await readTextFile("./textFile/transaction_type.txt");
      const res = await axios.post(`${url}/$batch`, msg, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `CompanyDB=${info.CompanyDB};B1SESSION=${info.B1SESSION};ROUTEID=${info.ROUTEID}`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      const data = {
        desc: "--TransactionType--",
        reqStatus: res.status,
        reqMsg: res.statusText,
        response: res.data,
      };
      console.log(data);
    } catch (e) {
      console.log("Error: ", e.response.data);
    }
  },
  batchTransactionStatusLog: async ({ info }) => {
    console.log(info);
    try {
      const msg = await readTextFile("./textFile/transaction_status_log.txt");
      const res = await axios.post(`${url}/$batch`, msg, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `CompanyDB=${info.CompanyDB};B1SESSION=${info.B1SESSION};ROUTEID=${info.ROUTEID}`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      const data = {
        desc: "--Transaction_Status_Log--",
        reqStatus: res.status,
        reqMsg: res.statusText,
        response: res.data,
      };
      console.log(data);
    } catch (e) {
      console.log("Error: ", e.response.data);
    }
  },
  batchPAD: async ({ info }) => {
    console.log(info);
    try {
      const msg = await readTextFile("./textFile/PAD.txt");
      const res = await axios.post(`${url}/$batch`, msg, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `CompanyDB=${info.CompanyDB};B1SESSION=${info.B1SESSION};ROUTEID=${info.ROUTEID}`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      const data = {
        desc: "--PAD--",
        reqStatus: res.status,
        reqMsg: res.statusText,
        response: res.data,
      };
      console.log(data);
    } catch (e) {
      console.log("Error: ", e.response.data);
    }
  },
  batchCAD: async ({ info }) => {
    console.log(info);
    try {
      const msg = await readTextFile("./textFile/CAD.txt");
      const res = await axios.post(`${url}/$batch`, msg, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `CompanyDB=${info.CompanyDB};B1SESSION=${info.B1SESSION};ROUTEID=${info.ROUTEID}`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      const data = {
        desc: "--CAD--",
        reqStatus: res.status,
        reqMsg: res.statusText,
        response: res.data,
      };
      console.log(data);
    } catch (e) {
      console.log("Error: ", e.response.data);
    }
  },
  batchPAR: async ({ info }) => {
    console.log(info);
    try {
      const msg = await readTextFile("./textFile/PAR.txt");
      const res = await axios.post(`${url}/$batch`, msg, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `CompanyDB=${info.CompanyDB};B1SESSION=${info.B1SESSION};ROUTEID=${info.ROUTEID}`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      const data = {
        desc: "--PAR--",
        reqStatus: res.status,
        reqMsg: res.statusText,
        response: res.data,
      };
      console.log(data);
    } catch (e) {
      console.log("Error: ", e.response.data);
    }
  },
  batchBusinessEntity: async ({ info }) => {
    console.log(info);
    try {
      const msg = await readTextFile("./textFile/business_entity.txt");
      const res = await axios.post(`${url}/$batch`, msg, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `CompanyDB=${info.CompanyDB};B1SESSION=${info.B1SESSION};ROUTEID=${info.ROUTEID}`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      const data = {
        desc: "--Business Entity--",
        reqStatus: res.status,
        reqMsg: res.statusText,
        response: res.data,
      };
      console.log(data);
    } catch (e) {
      console.log("Error: ", e.response.data);
    }
  },
  batchItems: async ({ info }) => {
    console.log(info);
    try {
      const msg = await readTextFile("./textFile/items.txt");
      const res = await axios.post(`${url}/$batch`, msg, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `CompanyDB=${info.CompanyDB};B1SESSION=${info.B1SESSION};ROUTEID=${info.ROUTEID}`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      const data = {
        desc: "--Items--",
        reqStatus: res.status,
        reqMsg: res.statusText,
        response: res.data,
      };
      console.log(data);
    } catch (e) {
      console.log("Error: ", e.response.data);
    }
  },
  batchRawMaterials: async ({ info }) => {
    console.log(info);
    try {
      const msg = await readTextFile("./textFile/raw_materials.txt");
      const res = await axios.post(`${url}/$batch`, msg, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `CompanyDB=${info.CompanyDB};B1SESSION=${info.B1SESSION};ROUTEID=${info.ROUTEID}`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      const data = {
        desc: "--Raw Materials--",
        reqStatus: res.status,
        reqMsg: res.statusText,
        response: res.data,
      };
      console.log(data);
    } catch (e) {
      console.log("Error: ", e.response.data);
    }
  },
  batchMicro: async ({ info }) => {
    try {
      const msg = await readTextFile("./textFile/micro.txt");
      const res = await axios.post(`${url}/$batch`, msg, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `CompanyDB=${info.CompanyDB};B1SESSION=${info.B1SESSION};ROUTEID=${info.ROUTEID}`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      const data = {
        desc: "--Micro--",
        reqStatus: res.status,
        reqMsg: res.statusText,
        response: res.data,
      };
      console.log(data);
    } catch (e) {
      console.log("Error: ", e.response.data);
    }
  },
  batchDrop: async ({ info }) => {
    console.log(info);
    try {
      const msg = await readTextFile("./textFile/DROP.txt");
      const res = await axios.post(`${url}/$batch`, msg, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `CompanyDB=${info.CompanyDB};B1SESSION=${info.B1SESSION};ROUTEID=${info.ROUTEID}`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      const data = {
        desc: "--DropTables--",
        reqStatus: res.status,
        reqMsg: res.statusText,
        response: res.data,
      };
      console.log(data);
    } catch (e) {
      console.log("Error: ", e.response.data);
    }
  },
  batchMicroDetails: async ({ info }) => {
    console.log(info);
    try {
      const msg = await readTextFile(
        "./textFile/micro_transaction_details.txt"
      );
      const res = await axios.post(`${url}/$batch`, msg, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `CompanyDB=${info.CompanyDB};B1SESSION=${info.B1SESSION};ROUTEID=${info.ROUTEID}`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      const data = {
        desc: "--Micro Transaction Details--",
        reqStatus: res.status,
        reqMsg: res.statusText,
        response: res.data,
      };
      console.log(data);
    } catch (e) {
      console.log("Error: ", e.response.data);
    }
  },
  batchLotNum: async ({ info }) => {
    console.log(info);
    try {
      const msg = await readTextFile("./textFile/lotnum.txt");
      const res = await axios.post(`${url}/$batch`, msg, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `CompanyDB=${info.CompanyDB};B1SESSION=${info.B1SESSION};ROUTEID=${info.ROUTEID}`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      const data = {
        desc: "--Micro Transaction Details--",
        reqStatus: res.status,
        reqMsg: res.statusText,
        response: res.data,
      };
      console.log(data);
    } catch (e) {
      console.log("Error: ", e.response.data);
    }
  },
  batchDeviceConfig: async ({ info }) => {
    console.log(info);
    try {
      const msg = await readTextFile("./textFile/device-config.txt");
      const res = await axios.post(`${url}/$batch`, msg, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `CompanyDB=${info.CompanyDB};B1SESSION=${info.B1SESSION};ROUTEID=${info.ROUTEID}`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      const data = {
        desc: "--Micro Transaction Details--",
        reqStatus: res.status,
        reqMsg: res.statusText,
        response: res.data,
      };
      console.log(data);
    } catch (e) {
      console.log("Error: ", e.response.data);
    }
  },
  batchCompany: async ({ info }) => {
    console.log(info);
    try {
      const msg = await readTextFile("./textFile/company.txt");
      const res = await axios.post(`${url}/$batch`, msg, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `CompanyDB=${info.CompanyDB};B1SESSION=${info.B1SESSION};ROUTEID=${info.ROUTEID}`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      const data = {
        desc: "--Company Info--",
        reqStatus: res.status,
        reqMsg: res.statusText,
        response: res.data,
      };
      console.log(data);
    } catch (e) {
      console.log("Error: ", e.response.data);
    }
  },
  batchNewWeights: async ({ info }) => {
    console.log(info);
    try {
      const msg = await readTextFile("./textFile/dr-new-weights.txt");
      const res = await axios.post(`${url}/$batch`, msg, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `CompanyDB=${info.CompanyDB};B1SESSION=${info.B1SESSION};ROUTEID=${info.ROUTEID}`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      const data = {
        desc: "--New Weights Info--",
        reqStatus: res.status,
        reqMsg: res.statusText,
        response: res.data,
      };
      console.log(data);
    } catch (e) {
      console.log("Error: ", e.response.data);
    }
  },
  batchDeleteAllUDT: async ({ info }) => {
    console.log(info);
    try {
      const msg = await readTextFile("./textFile/delete-all.txt");
      const res = await axios.post(`${url}/$batch`, msg, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `CompanyDB=${info.CompanyDB};B1SESSION=${info.B1SESSION};ROUTEID=${info.ROUTEID}`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      const data = {
        desc: "--Delete All UDT--",
        reqStatus: res.status,
        reqMsg: res.statusText,
        response: res.data,
      };
      console.log(data);
    } catch (e) {
      console.log("Error Delete All UDT: ", e.response.data);
    }
  },
  batchActivityLogs: async ({ info }) => {
    console.log(info);
    try {
      const msg = await readTextFile("./textFile/activity-logs.txt");
      const res = await axios.post(`${url}/$batch`, msg, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `CompanyDB=${info.CompanyDB};B1SESSION=${info.B1SESSION};ROUTEID=${info.ROUTEID}`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      const data = {
        desc: "--Activity Logs--",
        reqStatus: res.status,
        reqMsg: res.statusText,
        response: res.data,
      };
      console.log(data);
    } catch (e) {
      console.log("Error Activity Logs: ", e.response.data);
    }
  },
};

module.exports = batch;
